function parseString(str, objectMap) {
    var returnStr = str;

    console.log("Before parser starts", str, "\n");

    for (var prop in objectMap) {
        if (objectMap.hasOwnProperty(prop) && str.indexOf(prop) > -1) {
            returnStr = returnStr.replace(prop, objectMap[prop]);
        }
    }

    return returnStr;
}

module.exports = parseString;

