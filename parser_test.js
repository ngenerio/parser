var parser = require("./parser");

function unitTest() {
  var expected = "Thequickbrownfoxjumpedoverthelazydog";
  var actual = "";
  
  actual = parser("Theslowyellowdogjumpedoverthehappycat",
    { "slow": "quick", "yellow": "brown", "dog": "fox", "happy": "lazy", "cat": "dog" });
  
  if (actual !== expected) {
    return console.log("Test failed");
  }
  
  console.log(actual);
  console.log("Test passed");
}

unitTest();